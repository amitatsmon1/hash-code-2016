import logging
import math

logger = logging.getLogger("hashcode")


class DataSet:
    def __init__(self, rows, columns, drones, turns, max_load, weights):
        self.rows = rows
        self.columns = columns
        self.drones = drones
        self.turns = turns
        self.max_load = max_load
        self.weights = weights

    def __repr__(self):
        return "<DataSet> [rows: {}, columns: {}, drones: {}, turns: {}, max_load: {}]".format(self.rows, self.columns,
                                                                                               self.drones, self.turns,
                                                                                               self.max_load)


class Order:
    def __init__(self, index, cell, items):
        self.index = index
        self.cell = cell
        self.items = items

    def __repr__(self):
        return "<Order> [index: {}, cell: {}, items: {}]".format(self.index, self.cell, self.items)


class Cell:
    def __init__(self, location):
        self.x = location[0]
        self.y = location[1]

    def dist(self, other):
        return math.ceil(((self.x - other.x) ** 2 + (self.y - other.y) ** 2) ** 0.5)

    def __repr__(self):
        return "<Cell> [x: {}, y: {}]".format(self.x, self.y)


class Warehouse:
    def __init__(self, index, cell, items):
        self.index = index
        self.cell = cell
        self.items = items

    def __repr__(self):
        return "<Warehouse> [cell: {}, amount of each item: {}]".format(self.cell, self.items)

    def has(self, item_index):
        return self.items[item_index] > 0


class Drone:
    def __init__(self, index, cell, data_set):
        self.index = index
        self.cell = cell
        self.items = [0 for i in range(len(data_set.weights))]
        self.data_set = data_set
        self.weight = 0

    def add(self, item_index):
        self.weight += data_set.weight[item_index]
        self.item[item_index] += 1

    def can_add(self, item_index):
        return (self.weight + data_set.weight[item_index]) <= data_set.max_load

    def __repr__(self):
        return "<Warehouse> [cell: {}, amount of each item: {}]".format(self.cell, self.items)


def parse_input(file_path):
    line_number = 0
    with open(file_path, "r") as f:
        rows, columns, drones, turns, max_load = map(int, f.readline().split(" "))
        items_count = list(map(int, f.readline().split(" ")))
        weights = list(map(int, f.readline().split(" ")))  # size items_number
        data_set = DataSet(rows, columns, drones, turns, max_load, weights)
        warehouses_count = list(map(int, f.readline().split(" ")))[0]
        warehouses = []
        for i in range(warehouses_count):
            location = list(map(int, f.readline().split(" ")))
            cell = Cell(location)
            items = list(map(int, f.readline().split(" ")))
            warehouses.append(Warehouse(i, cell, items))
        order_count = list(map(int, f.readline().split(" ")))[0]
        orders = []
        for i in range(order_count):
            location = Cell(list(map(int, f.readline().split(" "))))
            items_number = list(map(int, f.readline().split(" ")))[0]
            items = list(map(int, f.readline().split(" ")))
            orders.append(Order(i, location, items))
            # line_number += 1
        assert len(orders) == order_count
        return data_set, warehouses, orders
