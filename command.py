class Command(object):
	def __init__(self, drone):
		self.drone = drone

	def __str__(self):
		raise NotImplemented("This is an abstract class")


class Load(Command):
	def __init__(self, drone, warehouse, product_type, amount):
		super(Load, self).__init__(drone)
		self.warehouse = warehouse
		self.product_type = product_type
		self.amount = amount

	def __str__(self):
		return "{drone} L {warehouse} {product_type} {amount}".format(drone=self.drone, warehouse=self.warehouse, amount=self.amount, product_type=self.product_type)

class Deliver(Command):
	def __init__(self, drone, costumer, product_type, amount):
		super(Deliver, self).__init__(drone)
		self.costumer = costumer
		self.amount = amount
		self.product_type = product_type

	def __str__(self):
		return "{drone} D {costumer} {product_type} {amount}".format(drone=self.drone, costumer=self.costumer, amount=self.amount, product_type=self.product_type)

class Unload(Command):
	def __init__(self, drone):
		super(Unload, self).__init__(drone)

	def __str__(self):
		return "{drone} U".format(drone=self.drone)

class Wait(Command):
	def __init__(self, drone):
		super(Wait, self).__init__(drone)

	def __str__(self):
		return "{drone} W".format(drone=self.drone)



# def gen_commands():
# 	commands = []
# 	num_of_turns = 0
# 	for order in orders:
# 		drone_number = order.index
# 		for item in order.items:
# 			commands.append(Load(drone=drone_number, warehouse, product_type=order))
# 	return commands

Commands = [Load(0,0,0,1), Load(0,0,1,1), Deliver(0,0,0,1), Load(0,1,2,1), Deliver(0,0,2,1), Load(1,1,2,1), Deliver(1,2,2,1), Load(1,0,0,1), Deliver(1,1,0,1)]

def write_output(output_file_name, commands):
	with open(output_file_name, "w") as f:
		f.write(str(len(commands)) + '\n')
		for command in commands:
			f.write(str(command) + '\n')

def main():
	write_output("output_test.txt", Commands)

if __name__ == '__main__':
	main()