# 298484feb2e5964713089ed7f0d61ba6ee6e9e34fba47c577bfb89d7ad15296b
from main import parse_input, Cell
from command import *

data_set, warehouses, orders = parse_input("inputs/busy_day.in")


# def get_items_from_warehouse(drone, warehouse, ):
def calc_weights(order):
    # print("order: {}".format(order.index))
    total = 0
    for item in order.items:
        # print("item: {} weight: {}".format(item, data_set.weights[item]))
        total += data_set.weights[item]
    return total


def calc_time(order, warehouse):
    return order.cell.dist(warehouse.cell)


# def res_73():
#     # data_set, warehouses, orders = parse_input("inputs/mother_of_all_warehouses.in")
#     print("turns: {}, orders: {} max_load: {}, drones: {}".format(data_set.turns, len(orders), data_set.max_load,
#                                                                   data_set.drones))
#     warehouse = warehouses[0]
#     commands = []
#     drone_count = 0
#     for order_number, order in enumerate(
#             sorted(orders, key=lambda order: calc_time(order, warehouse) * len(order.items))):
#         print("order: {} has: {} items, total weight: {}, total time: {}".format(order.index, len(order.items),
#                                                                                  calc_weights(order),
#                                                                                  calc_time(order, warehouse)))
#         # print("".format())
#         # drone_index = order_number % data_set.drones
#         total_load = data_set.max_load
#         done_command = []
#         for item_index in sorted(order.items, key=lambda item: data_set.weights[item]):
#             warehouse.items[item_index] -= 1
#             drone_index = drone_count % data_set.drones
#             if total_load - data_set.weights[item_index] >= 0:
#                 total_load -= data_set.weights[item_index]
#                 commands.append(Load(drone_index, warehouse.index, item_index, 1))
#                 done_command.append(Deliver(drone_index, order.index, item_index, 1))
#             else:
#                 # send drone away
#                 commands += done_command
#                 done_command = []
#                 drone_count += 1
#                 drone_index = drone_count % data_set.drones
#                 total_load = data_set.max_load - data_set.weights[item_index]
#
#                 commands.append(Load(drone_index, warehouse.index, item_index, 1))
#                 done_command.append(Deliver(drone_index, order.index, item_index, 1))
#
#             print("items: ", order.items)
#         commands += done_command
#         # num_of_turns += warehouse.cell.dist(order.cell) + 2
#         # break
#     write_output("outputs/as_much.txt", commands)


def item_in_house(house, order):
    total = 1
    for item in order.items:
        if house.items[item] > 0:
            total += 1
    return total


def get_house(item, order):
    for house in sorted(warehouses, key=lambda house: house.cell.dist(order.cell)):  # todo: sort
        if house.items[item] > 0:
            return house


def total_time(order):
    time = 0
    houses = sorted(warehouses, key=lambda house: house.cell.dist(order.cell))
    for item in order.items:
        for house in houses:
            if house.items[item] > 0:
                time += house.cell.dist(order.cell)
                break
    return time


def get_items_from_house(house, items):
    items_in_house = []
    for item in items:
        if house.items[item] > 0:
            items_in_house.append(item)

    res = []
    total_load = data_set.max_load
    for item in sorted(items_in_house, key=lambda item: data_set.weights[item]):
        if total_load - data_set.weights[item] >= 0:
            res.append(item)
            total_load -= data_set.weights[item]
    return res

def get_max_item_from_house(order):
    res_house = None
    res_items = None
    for house in warehouses:
        for item in order.items:
            if house.items
    return house, items



def busy_day():
    commands = []
    drone_count = 0
    print(len(orders))
    # for order_number, order in enumerate(sorted(orders, key=lambda order: calc_weights(order) * len(order.items))):
    for order_number, order in enumerate(sorted(orders, key=lambda order: total_time(order))):
        drone_count += 1
        drone_index = drone_count % data_set.drones

        print("order: {} has: {} items, total weight: {}".format(order.index, len(order.items), calc_weights(order)))

        total_load = data_set.max_load
        done_command = []

        max_items = []
        best_house = None
        while order.items:



            for item in max_items:
                assert best_house.items[item] > 0
                best_house.items[item] -=1
            # print("order.items: ", order.items)
            # print("max_items", max_items)
            # for t in max_items:
            #     print("t: ", t)
            #     print("items: ", order.items)
            #     order.items.remove(t)
            #     # best_house.items[item] -= 1
            #     warehouses[best_house.index].items[t] -= 1
            #     commands.append(Load(drone_index, best_house.index, t, 1))
            #     done_command.append(Deliver(drone_index, order.index, t, 1))
            # commands += done_command
    print(warehouses[9].items)
    write_output("outputs/as_much.txt", commands)


if __name__ == "__main__":
    busy_day()
    # Load - drone, warehouse, product_type, amount
    # Deliver - drone, costumer, product_type, amount):

    # optimize drone full capacity
    # optimize distance order and warehouse
    # optimize parallel orders
