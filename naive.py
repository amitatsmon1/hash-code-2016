from main import parse_input
from command import *

if __name__ == "__main__":
    data_set, warehouses, orders = parse_input("inputs/busy_day.in")
    print(data_set.turns)
    warehouse = warehouses[0]
    commands = []
    for order in sorted(orders, key=lambda order: len(order.items)):
        for item_index in order.items:
            for warehouse in warehouses:
                drone_index = order.index % data_set.drones
                if warehouse.has(item_index):
                    warehouse.items[item_index] -= 1
                    commands.append(Load(drone_index, warehouse.index, item_index, 1))
                    commands.append(Deliver(drone_index, order.index, item_index, 1))
                    # num_of_turns += warehouse.cell.dist(order.cell) + 2
                    break
    write_output("outputs/naive.txt", commands)


# Load - drone, warehouse, product_type, amount
# Deliver - drone, costumer, product_type, amount):