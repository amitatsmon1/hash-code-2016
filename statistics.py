from main import parse_input
from command import *

import matplotlib.pyplot as plt


BD = "busy_day.in" 
MOTHER = "mother_of_all_warehouses.in" 
REDU = "redundancy.in"

def duplicate_items_in_same_order():
    data_set, warehouses, orders = parse_input("inputs/" + REDU)
    for order in orders:
        if (len(set(order.items)) != len(order.items)):
            print(order.index)

if __name__ == "__main__":
    data_set, warehouses, orders = parse_input("inputs/" + BD)
    items = []
    print(len(warehouses))
    for warehouse in warehouses:
        for item_index, amount in enumerate(warehouse.items):
            for _ in range(amount):
                items.append(item_index)
        plt.hist(items, bins=items[-1])
        plt.show()


# Load - drone, warehouse, product_type, amount
# Deliver - drone, costumer, product_type, amount):